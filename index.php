<?php
require_once 'src/nSolutions/Filmweb.php';
require_once 'class_actors.php';
require_once 'class_movie.php';
require_once 'class_scrapper.php';
require_once 'class_template_card.php';
require_once 'class_template_form.php';
require_once 'class_database.php';

$filmweb = \nSolutions\Filmweb::instance();
$database = new Database();
//$database->showValueFromTable();

?>
<html>
    <head>
        <link href="style.css" type="text/css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="slick/slick.js"></script>
    </head>
    <body>    
        <div class="center">
            <div>
                <?php
                    echo "<h2>Top 10</h2>";
                    new TemplateForm();
                    $siteUrl = "http://www.filmweb.pl/ranking/film";
                    $Scrapper = new Scrapper($siteUrl);
                    $ScrapperIds = $Scrapper->getIdFromContent();
                    echo '<div class="ajax_select">';
                        foreach ($ScrapperIds as $Id) {
                            $Movie = new Movie($Id);
                            $database->setValueFilmsToTable($Movie);
                            new TemplateCard($Movie);
                        }
                    echo '</div>';
                ?>
            </div>

        </div>
    </body>
    <script>
        
        function ajax_loader() {
            $.get(
                "ajax_loader.php",
                {},
                function(data){
                    $('.card').prepend(data);
                }
            );
        }
        
        function ajax_select() {
            $.post(
                "ajax_select.php",
                {
                    multimedia : jQuery('#multimedia').val(),
                    platform : jQuery('#platform').val(),
                    data : jQuery('#data').val(),
                    type : jQuery('#type').val() 
                },
                function(data){
                    $('.ajax_select').html(data);
                    jQuery('.form__select').attr('disabled', false);
                }
            );
        }
  
        $('#multimedia').change(function(){
            var value = $(this).val();
            if(value == "game/"){
                $('#platform').show(500);
                $('#type').val("");
                $('#type').hide(500);
            } else {
                $('#platform').val("");
                $('#platform').hide(500);
                $('#type').show(500);
            }
        });
        
        $('.form__select').change(function(){
            jQuery('.form__select').attr('disabled', 'disabled');
            ajax_loader();
            ajax_select();
        });

        
        $(document).ready(function () {
            
            $('.center').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>
</html>
