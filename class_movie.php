<?php

    class Movie{
        private $FullInfo, 
                $Id, 
                $Image, 
                $Title, 
                $Genres, 
                $Time, 
                $DataPremier, 
                $VideoUrl, 
                $Actors,
                $Type;
     
        function __construct($id) {
            global $filmweb;
            $this->setIdMovie($id);
            $this->FullInfo = $filmweb->getFilmInfoFull($this->Id)->execute();
            $this->setDataPremierFromApi();
            $this->setGenresMovieFromApi();
            $this->setImageMovieFromApi();
            $this->setTitleMovieFromApi();
            $this->setVideoUrlFromApi();
            $this->setTimeFromApi();
            $this->setActorsFromClassActors();
            $this->setTypeFromApi();
        }
        
        function getIdMovie(){
            return $this->Id;
        }
        
        function getTitle(){
            return $this->Title;
        }
        
        function getImage(){
            return $this->Image;
        }
        
        function getDataPremier(){
            return $this->DataPremier;
        }
        
        function getVideoUrl(){
            return $this->VideoUrl;
        }
        
        function getTime(){
            return $this->Time;
        }
        
        function getGenres(){
            return $this->Genres;
        }
        
        function getActors(){
            return $this->Actors;
        }
        
        function getType(){
            return $this->Type;
        }
                
        function getFullInfo(){
            return $this->FullInfo;
        }
        
        function setIdMovie($id){
            $this->Id = $id;
        }
        
        function setTitleMovie($title){
            $this->Title = $title;
        }  
        
        function setImageMovie($image){
            $this->Image = $image;
        }
        
        function setGenresMovie($genres){
            $this->Genres = $genres;
        }
        
        function setTimeMovie($time){
            $this->Time = $time;
        }
        
        function setDataPremier($data){
            $this->DataPremier = $data;
        }
        
        function setVideoUrl($videourl){
            if($videourl){
                $this->VideoUrl = $videourl;
            } else {
                $this->VideoUrl = "javascript:void(0)";
            }
        }
        
        function setTypeMovie($type){
            $this->Type = $type;
        }
        
        function setActorsFromClassActors(){
            $allActors = new Actors($this->Id);
            $this->Actors = $allActors;
        }
                
        function setTitleMovieFromApi(){
            $this->setTitleMovie($this->FullInfo->title);
        }
        
        function setImageMovieFromApi(){
            $imageUrl = $this->FullInfo->imagePath;
            if($imageUrl){
                $imageUrl = str_replace(".3.jpg", ".6.jpg", $imageUrl);
                $headerUrl = get_headers($imageUrl);
                if ($headerUrl[0] == "HTTP/1.1 200 OK") {
                    $image = $imageUrl;
                } else {
                    $image = $this::setDefaultImage();
                }
            } else {
                $image = $this::setDefaultImage();
            }
            $this->setImageMovie($image);
        }
        
        function setGenresMovieFromApi() {
            $genresMovie = $this->FullInfo->genres;
            $genresMovie = str_replace(",", ", ", $genresMovie);
            $this->setGenresMovie($genresMovie);
        }
        
        function setTimeFromApi(){
            $time = $this->FullInfo->duration;
            if (!$time) {
                $time = '? ';
            }
            $this->setTimeMovie($time);
        }
        
        function setDataPremierFromApi(){
            $this->setDataPremier($this->FullInfo->premiereCountry);
        }
        
        function setVideoUrlFromApi(){
            $videoUrl = $this->FullInfo->video['videoUrl'];
            $videoUrl = str_replace("iphone", "480p", $videoUrl);
            $this->setVideoUrl($videoUrl);
        }
        
        function setTypeFromApi(){
            switch ($this->FullInfo->filmType) {
                case 0:
                    $this->setTypeMovie("film");
                    break;
                case 1:
                    $this->setTypeMovie("serial");
                    break;
                case 2:
                    $this->setTypeMovie("game");
                    break;
            }
            
        }
        
        static function setDefaultImage(){
            return '<img src="' . $_SERVER['HTTP_REFERER'] . '/img/filmweb-def.jpg"/>';
        }
    }