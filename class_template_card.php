<?php

class TemplateCard {

    function __construct($Movie) {
        ?>
        <div class='card'>
            <div class='card_left'>
                <img src="<?php echo $Movie->getImage(); ?>"/>
            </div>
            <div class='card_right'>
                <h1>
                    <?php 
                        if(strlen($Movie->getTitle()) > 45){
                            echo substr($Movie->getTitle(), 0, 45) . "..."; 
                        } else {
                            echo $Movie->getTitle(); 
                        }
                    ?>
                </h1>
                <div class='card_right__details'>
                    <ul>
                        <?php if($Movie->getDataPremier()){ ?>
                            <li><?php echo $Movie->getDataPremier(); ?></li>
                        <?php } ?>
                        <li><?php echo $Movie->getTime(); ?> min</li>
                        <li><?php echo $Movie->getGenres(); ?></li>
                    </ul>
                    <div class='card_right__button'>
                        <a href='<?php echo $Movie->getVideoUrl(); ?>' target='_blank'>ZOBACZ TRAILER</a>
                    </div>
                    <div class="actor">
                        <?php
                        $actors = $Movie->getActors();
                        $actorsSix = $actors->getFiveActors();

                        foreach ($actorsSix as $actor) {
                            if ($actor->personPhoto) {
                                ?>
                                <div class="actor__info">
                                    <img class="actor__foto" width="50" src="<?php echo $actor->personPhoto; ?>" alt="" />
                                </div>
                                <?php }
                            } ?>
                        <ul>
        <?php foreach ($actorsSix as $actor) { ?>
                                <li class="actor__name"><?php echo $actor->personName; ?></li>   
        <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div style='clear: both;'></div>
    <?php }

}
?>