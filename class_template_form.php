<?php

class TemplateForm {
    function __construct() {
        ?>
        <form class="form text-center">
            <select id="multimedia" class="form__select" name="multimedia">
                <option value="film/">Filmy</option>
                <option value="serial/">Seriale</option>
                <option value="game/">Gry</option>
            </select>
            <select style="display: none;" id="platform" class="form__select" name="platform">
                <option value="">Platforma</option>
                <option value="?platform=1">PC</option>
                <option value="?platform=2">PS4</option>
                <option value="?platform=4">Xbox One</option>
            </select>
            <select id="type" class="form__select" name="type">
                <option value="">Gatunek</option>
                <option value="Horror/12/">Horror</option>
                <option value="Komedia/13/">Komedia</option>
                <option value="Thriller/24/">Thriller</option>
                <option value="Komedia+rom./30/">Komedia rom.</option>
                <option value="Animacja/2/">Animacja</option>
                <option value="Sci-Fi/33/">Sci-Fi</option>
                <option value="Akcja/28/">Akcja</option>
                <option value="Fantasy/9/">Fantasy</option>
            </select>
            <select id="data" class="form__select form__input--data" name="data">
                <option value="">Data</option>
                <?php for($i = 2017; $i >= 2002; $i--){ ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </form>
        <?php
    }
}
