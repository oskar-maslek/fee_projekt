<?php

    class Actors{
        private $Actors; 
        
        function getActors(){
            return $this->Actors;
        }
        
        function getFiveActors(){
            $count = 0;
            $Sixactors = array();
            foreach ($this->Actors as $actor) {
                if ($count == 5){ 
                    break; 
                } else {
                    array_push($Sixactors, $actor);
                    $count++;
                }
            }
            return $Sixactors;
        }
        
        function setActors($actors){
            $this->Actors = $actors;
        }
        
        function __construct($idMovie){
            global $filmweb;
            $actors = $filmweb->getFilmPersons($idMovie, 6, 0)->execute();
            $this->setActors($actors);
        }
    } 
