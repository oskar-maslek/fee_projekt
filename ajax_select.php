<?php
require_once 'src/nSolutions/Filmweb.php';
require_once 'class_actors.php';
require_once 'class_movie.php';
require_once 'class_scrapper.php';
require_once 'class_template_card.php';
require_once 'class_database.php';

$filmweb = \nSolutions\Filmweb::instance();
$database = new Database();
?>

<?php
    $multimedia = $_POST['multimedia'];
    $platform = $_POST['platform'];
    $type = $_POST['type'];
    $data = $_POST['data'];
    $siteUrl = "http://www.filmweb.pl/ranking/" . $multimedia . $type . $data . $platform;
    
    $Scrapper = new Scrapper($siteUrl);
    $ScrapperIds = $Scrapper->getIdFromContent();
    echo '<div class="ajax_select">';
        foreach ($ScrapperIds as $Id) {
            $Movie = new Movie($Id);
            $database->setValueFilmsToTable($Movie);
            new TemplateCard($Movie);
        }
    echo '</div>';
?>

