<?php
    class Scrapper{
        private $Content;
        
        public function __construct($url) {
            $this->setContentFromUrl($url);
        }
        
        function getIdFromContent($count = 10){
            preg_match_all('/(data-id="[0-9]+")/', $this->Content, $Ids);
            $Ids = $Ids[0];
            $Ids = array_slice($Ids, 0, $count);
            for ($i = 0; $i < count($Ids); $i++) {
                preg_match('/[0-9]+/', $Ids[$i], $Id);
                $Ids[$i] = $Id[0];
            }
            return $Ids;
        }
        
        function showId(){
            echo '<pre>';
            print_r($this->getIdFromContent());
            echo '</pre>';
        }
        
        function setContentFromUrl($url){
            $this->Content = file_get_contents($url); 
        }
    }

