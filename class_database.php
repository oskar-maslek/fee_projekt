<?php

class Database {
    var $host = "localhost";
    var $username = "root";
    var $passwd = "";
    var $dbname = "filmweb_database";
    var $database;
    
    function __construct(){
        $this->database = new mysqli(
            $this->host, 
            $this->username, 
            $this->passwd, 
            $this->dbname
        );
        $this->database->set_charset("utf8");
        
        if($this->database->connect_errno){
            echo $this->database->connect_errno;
        }
    }
    
    function getActorsIDs($ObjMovie){
        $i = 1;
        $actors = "";
        foreach($ObjMovie->getActors()->getFiveActors() as $Person){
            $actors .= $Person->personId;
            if(count($ObjMovie->getActors()->getFiveActors()) > $i){
                $actors .= ",";
                $i++;
            }
        }  
        return $actors;
    }

    function setValueFilmsToTable($ObjMovie){
        //print_r($ObjMovie->getType());
        $id_film = $ObjMovie->getIdMovie();
        $title = $ObjMovie->getTitle();
        $cover = $ObjMovie->getImage();
        $type = $ObjMovie->getType();
        $date = $ObjMovie->getDataPremier();
        $time = $ObjMovie->getTime();
        $trailer = $ObjMovie->getVideoUrl();
        $genres = $ObjMovie->getGenres();
        $actors = $this->getActorsIDs($ObjMovie);
        
        $this->setValueActorsToTable($ObjMovie);
        
        $result = $this->database->query(
            "INSERT INTO top_$type "
            . "(id_film, title, cover, type, date, time, trailer, genres, actors) "
            . "VALUES ('$id_film', '$title', '$cover', '$type', '$date', '$time', '$trailer', '$genres', '$actors')"
        );
        if(!$result){
            //echo "Błąd dodania nowego rekordu.";
            //print_r($this->database->error);
        }
    }
    
    function setValueActorsToTable($ObjMovie){
        foreach($ObjMovie->getActors()->getFiveActors() as $Actor){
            $id_actor = $Actor->personId;
            $name = $Actor->personName;
            $image = $Actor->personPhoto;

            $result = $this->database->query("INSERT INTO top_actors "
                . "(id_actor, name, image) "
                . "VALUES ('$id_actor', '$name', '$image')"
            );
        }
        
        if(!$result){
            //echo "Błąd dodania nowego rekordu.";
            //print_r($this->database->error);
        }
    }
    
    function showValueFromTable(){
        echo "<pre>";
        print_r($this->database->query("SELECT * FROM top_film")->fetch_all());
        echo "</pre>";
    }
    
    function __destruct(){
        $this->database->close();
    }
}